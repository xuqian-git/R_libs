Package: sommer
Type: Package
Title: Solving Mixed Model Equations in R
Version: 4.1.5
Date: 2021-12-13
Author: Giovanny Covarrubias-Pazaran
Maintainer: Giovanny Covarrubias-Pazaran <cova_ruber@live.com.mx>
Description: Structural multivariate-univariate linear mixed model solver for estimation of multiple random effects with unknown variance-covariance structures (e.g., heterogeneous and unstructured) and known covariance among levels of random effects (e.g., pedigree and genomic relationship matrices) (Covarrubias-Pazaran, 2016 <doi:10.1371/journal.pone.0156744>; Maier et al., 2015 <doi:10.1016/j.ajhg.2014.12.006>). REML estimates can be obtained using the Direct-Inversion Newton-Raphson and Direct-Inversion Average Information algorithms. Designed for genomic prediction and genome wide association studies (GWAS), particularly focused in the p > n problem (more coefficients to estimate than observations). Spatial models can also be fitted using the two-dimensional spline functionality available in sommer.
Depends: R (>= 3.5.0), Matrix (>= 1.1.1), methods, stats, MASS,
        lattice, crayon
License: GPL (>= 2)
Imports: Rcpp (>= 0.12.19)
LinkingTo: Rcpp, RcppArmadillo, RcppProgress
Suggests: rmarkdown, knitr, plyr, parallel, orthopolynom, testthat (>=
        3.0.0)
VignetteBuilder: knitr
Config/testthat/edition: 3
NeedsCompilation: yes
Packaged: 2021-12-14 23:14:02 UTC; giovannycovarrubias
Repository: CRAN
Date/Publication: 2021-12-15 04:00:02 UTC
Built: R 3.6.3; x86_64-pc-linux-gnu; 2022-02-15 17:00:55 UTC; unix
