Package: waves
Title: Vis-NIR Spectral Analysis Wrapper
Version: 0.2.3
Authors@R: 
    c(person(given = "Jenna",
             family = "Hershberger",
             role = c("aut", "cre"),
             email = "jmh579@cornell.edu",
             comment = c(ORCID = "0000-0002-3147-6867")),
      person(given = "Michael",
             family = "Gore",
             role = "ths"),
      person(given = "NSF BREAD IOS-1543958",
             role = "fnd"))
Maintainer: Jenna Hershberger <jmh579@cornell.edu>
Description: Originally designed application in the context of
    resource-limited plant research and breeding programs, 'waves'
    provides an open-source solution to spectral data processing and model
    development by bringing useful packages together into a streamlined
    pipeline.  This package is wrapper for functions related to the
    analysis of point visible and near-infrared reflectance measurements.
    It includes visualization, filtering, aggregation, preprocessing,
    cross-validation set formation, model training, and prediction
    functions to enable open-source association of spectral and reference
    data. This package is documented in a peer-reviewed manuscript in the
    Plant Phenome Journal <doi:10.1002/ppj2.20012>.  Specialized
    cross-validation schemes are described in detail in Jarquín et al.
    (2017) <doi:10.3835/plantgenome2016.12.0130>. Example data is from
    Ikeogu et al. (2017) <doi:10.1371/journal.pone.0188918>.
License: MIT + file LICENSE
URL: https://github.com/GoreLab/waves
BugReports: https://github.com/GoreLab/waves/issues
Depends: R (>= 3.5)
Imports: caret, dplyr, ggplot2, lifecycle, magrittr, pls, prospectr,
        randomForest, readr, rlang, scales, spectacles, stringr,
        tibble, tidyr (>= 1.0), tidyselect
Suggests: testthat (>= 2.1.0), knitr, rmarkdown
Encoding: UTF-8
LazyData: true
RoxygenNote: 7.1.2
VignetteBuilder: knitr, rmarkdown
NeedsCompilation: no
Packaged: 2022-03-06 21:22:42 UTC; Jenna
Author: Jenna Hershberger [aut, cre] (<https://orcid.org/0000-0002-3147-6867>),
  Michael Gore [ths],
  NSF BREAD IOS-1543958 [fnd]
Repository: CRAN
Date/Publication: 2022-03-07 07:50:02 UTC
Built: R 3.6.3; ; 2022-03-17 04:05:43 UTC; unix
