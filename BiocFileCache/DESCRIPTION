Package: BiocFileCache
Title: Manage Files Across Sessions
Version: 1.10.2
Authors@R: c(person("Lori", "Shepherd",
      email = "lori.shepherd@roswellpark.org",
      role = c("aut", "cre")),
    person("Martin", "Morgan",
      email = "martin.morgan@roswellpark.org",
      role = "aut"))
Description: This package creates a persistent on-disk cache of files
    that the user can add, update, and retrieve. It is useful for
    managing resources (such as custom Txdb objects) that are costly
    or difficult to create, web resources, and data files used across
    sessions.
Depends: R (>= 3.4.0), dbplyr (>= 1.0.0)
Imports: methods, stats, utils, dplyr, RSQLite, DBI, rappdirs, curl,
        httr
BugReports: https://github.com/Bioconductor/BiocFileCache/issues
DevelopmentURL: https://github.com/Bioconductor/BiocFileCache
License: Artistic-2.0
Encoding: UTF-8
LazyData: true
RoxygenNote: 6.1.1
biocViews: DataImport
VignetteBuilder: knitr
Suggests: testthat, knitr, BiocStyle, rmarkdown, rtracklayer
git_url: https://git.bioconductor.org/packages/BiocFileCache
git_branch: RELEASE_3_10
git_last_commit: e6d2a47
git_last_commit_date: 2019-11-08
Date/Publication: 2019-11-08
NeedsCompilation: no
Packaged: 2019-11-09 03:18:50 UTC; biocbuild
Author: Lori Shepherd [aut, cre],
  Martin Morgan [aut]
Maintainer: Lori Shepherd <lori.shepherd@roswellpark.org>
Built: R 3.6.3; ; 2022-02-24 20:43:55 UTC; unix
